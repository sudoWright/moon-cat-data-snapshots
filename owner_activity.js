require('dotenv').config();
const { MoonCatAcclimator, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');
const request = require('./lib/request');
const { initializeFile, writeFileAsync } = require('./lib/fileUtils');
const { FORK_BLOCK, ETHERSCAN_API_KEY } = process.env;

const SECONDS_IN_DAY = 60 * 60 * 24;
const ACTIVITY_FILE = 'address_activity.json';

/**
 * Get a list of transactions for the target account
 * Uses Etherscan API for querying data about the specified address
 */
async function getTransactionsForAddress(address) {
  let rs = await request(`https://api.etherscan.io/api?module=account&action=txlist&address=${address}&startblock=4134866&sort=desc&apikey=${ETHERSCAN_API_KEY}`);
  if (rs.httpStatusCode != 200) {
    console.error('Etherscan query error');
    console.log(rs);
    process.exit(1);
  }
  return JSON.parse(rs.body.toString('utf8')).result;
}

/**
 * Record most recent on-chain activity for a set of addresses
 * Iterate through all addresses that own a MoonCat at the current FORK_BLOCK,
 * and for each one, determine when their most recent outbound transaction happened.
 * Data is saved to ACTIVITY_FILE with the timestamp of their most recent transaction.
 *
 * If an address has no outbound transactions but does have inbound transactions,
 * the date of the most recent inbound transaction is used.
 */
(async () => {
  const MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  const MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;
  const NFTX_MOONCAT_POOL_ADDRESS = '0x98968f0747E0A261532cAcC0BE296375F5c08398';
  const NFTX_MCAT17_POOL_ADDRESS = '0xA8b42C82a628DC43c2c2285205313e5106EA2853';
  const OLD_WRAPPER_ADDRESS = '0x7C40c393DC0f283F318791d746d894DdD3693572';

  const WILDMOONCAT_OWNERS_FILE = `${MOONCATRESCUE_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  let wildmooncat_owners = require('./' + WILDMOONCAT_OWNERS_FILE);
  let mooncat_owners = require('./' + MOONCAT_OWNERS_FILE);

  // Combine wild and acclimated owners into one list
  let merged_owners = Object.assign({}, mooncat_owners);
  Object.keys(wildmooncat_owners).forEach(ownerAddress => {
    if (typeof merged_owners[ownerAddress] == 'undefined') {
      merged_owners[ownerAddress] = wildmooncat_owners[ownerAddress];
    } else {
      merged_owners[ownerAddress] = merged_owners[ownerAddress].concat(wildmooncat_owners[ownerAddress])
    }
  });
  // Remove pool addresses
  delete merged_owners[NFTX_MOONCAT_POOL_ADDRESS];
  delete merged_owners[NFTX_MCAT17_POOL_ADDRESS];
  delete merged_owners[OLD_WRAPPER_ADDRESS];

  const owners = Object.keys(merged_owners);

  initializeFile(ACTIVITY_FILE);
  let ownerActivity = require('./' + ACTIVITY_FILE);
  const now = new Date();
  console.log(`Parsing ${owners.length} owner addresses...`);
  for (let i = 0; i < owners.length; i++) {
    const ownerAddress = owners[i];
    let txList = (await getTransactionsForAddress(ownerAddress));
    if (txList.length == 0) {
      console.log('No transactions for address', ownerAddress);
      ownerActivity[ownerAddress] = 0;
      continue;
    }
    let outboundTx = txList.filter(tx => {
      return tx.from.toLowerCase() == ownerAddress.toLowerCase();
    });
    let latestTx;
    if (outboundTx.length == 0) {
      console.log('No outbound transactions for address', ownerAddress);
      latestTx = txList[0];
    } else {
      latestTx = outboundTx[0];
    }
    let txDate = new Date(latestTx.timeStamp * 1000);
    let deltaSeconds = Math.floor((now.getTime() - txDate.getTime()) / 1000);
    ownerActivity[ownerAddress] = Number(latestTx.timeStamp);
    console.log(owners.length - i, ownerAddress, txDate.toISOString(), (deltaSeconds / SECONDS_IN_DAY).toFixed(2) + ' days');
    if (i % 10 == 0) {
      await writeFileAsync(ACTIVITY_FILE, JSON.stringify(ownerActivity, null, 2));
    }
  }
  await writeFileAsync(ACTIVITY_FILE, JSON.stringify(ownerActivity, null, 2));
})();