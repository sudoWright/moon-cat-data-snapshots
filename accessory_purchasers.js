require('dotenv').config();
const { writeFileAsync } = require('./lib/fileUtils');
const { ACC_MANAGER } = process.env;

const accessoryTraits = require('./0x8d33303023723dE93b213da4EB53bE890e747C63_16517000_traits.json')
const accessoryPurchases = require('./accessory_purchases.json')

/**
 * Given an Ethereum address, find all accessories that are managed by that account.
 * Then, find all addresses that purchased one of those accessories.
 */
async function main() {
  if (typeof ACC_MANAGER == 'undefined' || ACC_MANAGER == '') {
    console.error('No ACC_MANAGER specified');
    process.exit(1);
  }

  const PURCHASERS_FILE = `${ACC_MANAGER}_${accessoryPurchases.lastBlock}_buyers.json`;
  let managedAccessories = accessoryTraits.filter(acc => acc.manager.toLowerCase() == ACC_MANAGER.toLowerCase());
  console.log(`Address ${ACC_MANAGER} manages ${managedAccessories.length} accessories...`);

  let purchasers = {};
  managedAccessories.forEach(acc => {
    //console.log(`Parsing accessory ${acc.id}...`);
    let owners = accessoryPurchases.purchases[acc.id];
    for (const moonCatId in owners) {
      purchasers[owners[moonCatId].sender] = {
        accessory: acc.id,
        mooncat: parseInt(moonCatId),
        block: owners[moonCatId].block,
        timestamp: owners[moonCatId].timestamp,
        tx: owners[moonCatId].tx
      }
    }
  })

  await writeFileAsync(PURCHASERS_FILE, JSON.stringify(purchasers, null, 2));

  console.log("\n");
  console.log('Accessories:', managedAccessories.map(acc => acc.id).join(', '));
  console.log("\n");
  console.log(`Purchasers: (${Object.keys(purchasers).length})`);
  console.log(Object.keys(purchasers).sort().join("\n"));
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
