// SPDX-License-Identifier: AGPL-3.0
pragma solidity ^0.8.9;

import "@mooncatrescue/contracts/IMoonCatLootprints.sol";
import "@mooncatrescue/contracts/IMoonCatRescue.sol";

interface ILootprints {
  function getDetails(uint256 lootprintId) external view returns (
    uint8 status,
    string memory class,
    uint8 bays,
    string memory colorName,
    string memory shipName,
    address tokenOwner,
    uint32 seed
  );
}

contract BulkOwners {

  function ownersOf (address tokenAddress, uint256 tokenSearchStart, uint256 tokenSearchEnd) public view returns (address[] memory tokenOwners) {
    unchecked {
      require(tokenSearchEnd >= tokenSearchStart, "Search parameters out of order");
      IERC721 token = IERC721(tokenAddress);
      tokenOwners = new address[](tokenSearchEnd - tokenSearchStart + 1);
      for (uint256 i = tokenSearchStart; i < tokenSearchEnd; i++) {
        try token.ownerOf(i) returns (address tokenOwner) {
          tokenOwners[i - tokenSearchStart] = tokenOwner;
        } catch {
          // Leave owner as zero
        }
      }
    }
  }

  function ownersOfWildMoonCats (uint256 tokenSearchStart, uint256 tokenSearchEnd) public view returns (address[] memory tokenOwners) {
    unchecked {
      require(tokenSearchEnd >= tokenSearchStart, "Search parameters out of order");
      IMoonCatRescue MCR = IMoonCatRescue(0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6);
      tokenOwners = new address[](tokenSearchEnd - tokenSearchStart + 1);
      for (uint256 i = tokenSearchStart; i < tokenSearchEnd; i++) {
        try MCR.catOwners(MCR.rescueOrder(i)) returns (address tokenOwner) {
          tokenOwners[i - tokenSearchStart] = tokenOwner;
        } catch {
          // Leave owner as zero
        }
      }
    }
  }

  /**
   * Don't use! Included for reference/comparison.
   * This method is much slower than using the "ownersOf" method above, for the lootprints contract
   */
  function ownersOfLootprints (uint256 tokenSearchStart, uint256 tokenSearchEnd) public view returns (address[] memory tokenOwners) {
    unchecked {
      require(tokenSearchEnd >= tokenSearchStart, "Search parameters out of order");
      IMoonCatLootprints lootprints = IMoonCatLootprints(0x1e9385eE28c5C7d33F3472f732Fb08CE3ceBce1F);
      tokenOwners = new address[](tokenSearchEnd - tokenSearchStart + 1);
      for (uint256 i = tokenSearchStart; i < tokenSearchEnd; i++) {
        try lootprints.getDetails(i) returns (uint8 status, string memory, uint8, string memory, string memory, address tokenOwner, uint32) {
          if (status == 3) {
            tokenOwners[i - tokenSearchStart] = tokenOwner;
          }
        } catch {
          // Leave owner as zero
        }
      }
    }
  }

}