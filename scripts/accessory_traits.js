const { ethers } = require('hardhat');
const { writeFileAsync } = require('../lib/fileUtils');
const { MoonCatAccessories } = require('@mooncatrescue/contracts/moonCatUtils');
const { FORK_BLOCK } = process.env;

async function main() {
  const accounts = await ethers.getSigners();
  const MULTI = new ethers.Contract('0xcA11bde05977b3631167028862bE2a173976CA11', [
    // https://github.com/mds1/multicall
    'function aggregate3(tuple(address target, bool allowFailure, bytes callData)[] calls) external view returns (tuple(bool success, bytes returnData)[] returnData)',
  ], accounts[0]);
  const ACCESSORIES = await MoonCatAccessories;
  const TRAITS_FILE = `${ACCESSORIES.address}_${FORK_BLOCK}_traits.json`;

  let accessoryCount = (await ACCESSORIES.totalAccessories()).toNumber();
  console.log('Total Accessories:', accessoryCount);

  let accessory_traits = [];
  const CHUNK_SIZE = 50;
  for (let i = 0; i < accessoryCount; i += CHUNK_SIZE) {
    console.log(`At ${i}...`);
    let callDatas = [];
    for (let j = 0; j < CHUNK_SIZE; j++) {
      if (i + j >= accessoryCount) break;
      let cd = (await ACCESSORIES.populateTransaction.accessoryInfo(i + j)).data;
      callDatas.push({
        target: ACCESSORIES.address,
        allowFailure: false,
        callData: cd
      });
      cd = (await ACCESSORIES.populateTransaction.accessoryImageData(i + j)).data;
      callDatas.push({
        target: ACCESSORIES.address,
        allowFailure: false,
        callData: cd
      });
    }
    let rs = await MULTI.aggregate3(callDatas);
    for (let j = 0; j < rs.length; j++) {
      let r = rs[j];
      if (r.success !== true) {
        console.error('Trait fetch error');
        console.log(r);
        process.exit(1);
      }
      if (j % 2 == 0) {
        // Even-numbered results are the main accessory information
        let accessoryId = i + j/2;
        let accessoryInfo = ACCESSORIES.interface.decodeFunctionResult('accessoryInfo', r.returnData);
        let name = Buffer.from(accessoryInfo.name.substr(2), 'hex').toString('utf-8');
        let nullPos = name.indexOf(String.fromCharCode(0));
        if (nullPos >= 0) {
          name = name.substring(0, nullPos);
        }
        let data = {
          id: accessoryId,
          totalSupply: accessoryInfo.totalSupply,
          availableSupply: accessoryInfo.availableSupply,
          name: name,
          manager: accessoryInfo.manager,
          metabyte: accessoryInfo.metabyte,
          availablePalettes: accessoryInfo.availablePalettes,
          positions: accessoryInfo.positions,
          availableForPurchase: accessoryInfo.availableForPurchase,
          price: accessoryInfo.price.toString()
        };
        accessory_traits.push(data)
      } else {
        // Odd-numbered results are the image information
        let accessoryId = i + (j-1)/2;
        let img = ACCESSORIES.interface.decodeFunctionResult('accessoryImageData', r.returnData);
        accessory_traits[accessoryId].palettes = img.palettes.slice(0, accessory_traits[accessoryId].availablePalettes);
        accessory_traits[accessoryId].width = img.width;
        accessory_traits[accessoryId].height = img.height;
        accessory_traits[accessoryId].idat = img.IDAT;

        delete accessory_traits[accessoryId].availablePalettes;
      }
    }
    await writeFileAsync(TRAITS_FILE, JSON.stringify(accessory_traits, null, 2));
  }
  await writeFileAsync(TRAITS_FILE, JSON.stringify(accessory_traits, null, 2));
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
