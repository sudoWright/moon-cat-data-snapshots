require('dotenv').config();
const fs = require('fs');
const { ethers } = require('hardhat');
const { MoonCatAcclimator, MoonCatLootprints, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');

const { TEST_ID } = process.env;

const BATCH_START = 0;
const BATCH_END = 100;

// Test timing difference between JS looping through owners vs. BulkOwners looping
async function main() {
  const RESCUE = await MoonCatRescue;
  const ACCLIMATOR = await MoonCatAcclimator;
  const LOOTPRINTS = await MoonCatLootprints;

  const BulkOwners = await ethers.getContractFactory('BulkOwners');
  const BULK = await BulkOwners.deploy();

  // Clear cache
  fs.rmSync('./cache/hardhat-network-fork', { recursive: true });

  switch (TEST_ID) {
    case 'wild batch': {
      // Use the BULK custom contract to iterate over the batch
      console.time(TEST_ID);
      let owners = await BULK.ownersOfWildMoonCats(BATCH_START, BATCH_END);
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'acclimated batch': {
      // Use the BULK custom contract to iterate over the batch
      console.time(TEST_ID);
      let owners = await BULK.ownersOf(ACCLIMATOR.address, BATCH_START, BATCH_END);
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'lootprints ownerof batch': {
      // Use the BULK custom contract to iterate over the batch
      console.time(TEST_ID);
      let owners = await BULK.ownersOf(LOOTPRINTS.address, BATCH_START, BATCH_END);
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'lootprints details batch': {
      // Use the BULK custom contract to iterate over the batch
      console.time(TEST_ID);
      let owners = await BULK.ownersOfLootprints(BATCH_START, BATCH_END);
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }

    case 'wild loop': {
      // Directly call the MoonCatRescue contract, getting each MoonCat's ID and then
      // looking up their Owner
      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        let catId = await RESCUE.rescueOrder(i);
        let ownerAddress = await RESCUE.catOwners(catId);
        owners[i] = ownerAddress;
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'wild loop2': {
      // Loop through the MoonCatRescue contract using cached MoonCat hex IDs,
      // so only making one ownership query
      const MOONCATRESCUE_MEMORY_SLOTS_FILE = `${RESCUE.address}_memory.json`;
      let moonCatRescue_memory = require('../' + MOONCATRESCUE_MEMORY_SLOTS_FILE);

      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        const moonCatData = moonCatRescue_memory[i];
        let ownerAddress = await RESCUE.catOwners(moonCatData.catId);
        owners[moonCatData.rescueOrder] = ownerAddress
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'acclimated loop': {
      // Directly call the MoonCatAcclimator contract
      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        try {
          let owner = await ACCLIMATOR.ownerOf(i);
          owners[i] = owner;
        } catch (err) {
          if (err.error.toString().indexOf('That MoonCat is not wrapped') < 0) {
            console.error('ERROR parsing MoonCat ' + i);
            console.log(err);
          }
        }
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'lootprints details loop': {
      // Directly call the MoonCatLootprints contract, using the 'getDetails' function
      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        let details = await LOOTPRINTS.getDetails(i);
        if (details.status == 3) {
          owners[i] = details.tokenOwner;
        } else {
          owners[i] = ethers.constants.AddressZero;
        }
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }
    case 'lootprints ownerof loop': {
      // Directly call the MoonCatLootprints contract, using the 'ownerOf' function
      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        try {
          let owner = await LOOTPRINTS.ownerOf(i);
          owners[i] = owner;
        } catch (err) {
          if (err.error.toString().indexOf('ERC721: operator query for nonexistent token') < 0) {
            console.error('ERROR parsing lootprint ' + i);
            console.log(err);
          }
        }
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }

    case 'wild memory': {
      // Directly call the MoonCatRescue contract, looking up raw memory values for each MoonCat
      const MOONCATRESCUE_MEMORY_SLOTS_FILE = `${RESCUE.address}_memory.json`;
      let moonCatRescue_memory = require('../' + MOONCATRESCUE_MEMORY_SLOTS_FILE);
      let abiCoder = ethers.utils.defaultAbiCoder;

      console.time(TEST_ID);
      let owners = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        const moonCatData = moonCatRescue_memory[i];
        const storageData = await ethers.provider.getStorageAt(RESCUE.address, moonCatData.ownerSlot);
        owners[moonCatData.rescueOrder] = abiCoder.decode(['address'], storageData)[0];
      }
      console.timeEnd(TEST_ID);
      //console.log(owners);
      break;
    }

    case 'wild multi': {
      // Use the generic "Multicall" contract to batch up queries.
      const accounts = await ethers.getSigners();
      const MULTI = new ethers.Contract('0xcA11bde05977b3631167028862bE2a173976CA11', [
        // https://github.com/mds1/multicall
        'function aggregate3(tuple(address target, bool allowFailure, bytes callData)[] calls) external view returns (tuple(bool success, bytes returnData)[] returnData)',
      ], accounts[0]);

      console.time(TEST_ID);
      let callDatas = [];
      for (let i = BATCH_START; i < BATCH_END; i++) {
        let cd = (await RESCUE.populateTransaction.rescueOrder(i)).data;
        callDatas.push({
          target: RESCUE.address,
          allowFailure: false,
          callData: cd
        });
      }
      let ids = await MULTI.aggregate3(callDatas);

      callDatas = [];
      for (let i = 0; i < ids.length; i++) {
        let catId = RESCUE.interface.decodeFunctionResult('rescueOrder', ids[i].returnData)[0];
        let cd = (await RESCUE.populateTransaction.catOwners(catId)).data;
        callDatas.push({
          target: RESCUE.address,
          allowFailure: false,
          callData: cd
        });
      }
      let owners = await MULTI.aggregate3(callDatas);
      let rs = owners.map(rs => {
        return RESCUE.interface.decodeFunctionResult('catOwners', rs.returnData)[0];
      });
      console.timeEnd(TEST_ID);
      //console.log(rs);
      break;
    }

    default: {
      throw new Error(`Unknown test "${TEST_ID}"`);
    }
  }
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
