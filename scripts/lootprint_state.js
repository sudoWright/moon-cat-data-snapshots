require('dotenv').config();
const { ethers } = require('hardhat');
const { MoonCatLootprints } = require('@mooncatrescue/contracts/moonCatUtils');

let MOONCAT_LOOTPRINTS_ADDRESS = false;

// https://medium.com/better-programming/solidity-storage-variables-with-ethers-js-ca3c7e2c2a64

// lootprints memory slot numbering:
// ===================================
//     0  : Acclimator Address
//     1  : MoonCatRescue Address
//     2  : lootprints Metadata Address
//   3-6  : honorifics strings (4)
//   7-38 : adjectives strings (32)
//  39-53 : mods strings (15)
//  54-85 : main strings (32)
//  86-101: designations strings (16)
// 102-501: ColorTable (400-length array of 32-byte words)
//     502: Owner address
//     502: frozen boolean
//     502: minting open boolean
//     502: reveal count (uint8)
//     503: price (uint256)
// 504-603: No-charge list (100-length array of 32-byte words)
// 604-623: Reveal blockhashes (20-length array of 32-byte words)


/*
Reveal Blockhashes:

0604 0x4b070ab0f3977ed60155863326e18aa33893eea2994bef7dbf993b7f5bc056f4
0605 0xf4051b701f9e5f413456292a42f1c13492d4177859c300fa61cdbaef23b7defb
0606 0x29909d5dbd1d3e1ecf28c26ad7158b0e9e420066392523bec53296a9c9730159
0607 0xce27d7465d3ca6ffcc475092bf41ac170605fc6fb6a4df3f36894e281891bc38
0608 0x3616afa76e65c2c0e8357e14178429d3f6a2b2119c1599021f269b9d8a5bc63b
0609 0x2161b097abb27883e7529f222980f3e09a59b6a473b56cccf45fc84e8d7155f0
0610 0x24c8dd6c765a265d612be4ac67d4f89190c99e4f7b4cdc0f46e65302578e66ea
0611 0x07133d9d62e47e7309749ee6f52ffc11a7235e5724e3fa675f084e39df41c756
0612 0x6e009c9a36f53d808e34ea46eb912a5f097bd2d6036163ee8ceba220364e5cf6
0613 0x44010cc875248257026b952ee5d2cce15bbd099d23abd6e4ea0245d8c1117312
0614 0x530e72cd291578d2ca6bddd054bb89638d6e364a92cc1705c619b20c136da6c7
0615 0xaa1702c4ef7a85d68d7f5f363650d07e00a263ea024cf9d7cd69334797412b08
0616 0x771823c421180482c57658c880ff3e732c70d064b66c60b9be67c0f7296fc948
0617 0x2a331e173389e7a49876ce1321a32fdf6961686ad6edaa29bbfa4fefe0e25766
0618 0x14814e199c8230013fb2a485aa809c23b79fd92dccbf7b333ca45f9ad859e4a8
0619 0x9cd8ef618851f637da4db0bacba7a746ccbc904d48ffaef2a5d4a414bcab6727
0620 0x6c190cf2897edb23131e1ccaac6fa3db757ced227a658961691fee48c1293a0f
0621 0x5f45f0272a59823b63c4502c5644c063a30ebc67d74f991f2e4438a6d6cac6f0
0622 0x2c8cf5baca864e851c97a5c61239889107bea8499d1422815c4d4c502bfa8622
0623 0x7aca66f97ef29fc7c910aec95a515e8f853a5d0829c24c0ff08e342a2953da22
*/

async function getSlotData(slot) {
  const paddedSlot = ethers.utils.hexZeroPad(slot, 32);
  const storageData = await ethers.provider.getStorageAt(MOONCAT_LOOTPRINTS_ADDRESS, paddedSlot);
  return storageData;
}

function asString(data) {
  let trimmed = ethers.BigNumber.from(data).and(ethers.constants.MaxInt256.sub(255)).toHexString();
  try {
    return ethers.utils.toUtf8String(trimmed).replace(/\x00/g, '');
  } catch (err) {
    return '';
  }
}

/**
 * Iterate through the memory slots around where the reveal blockhashes should be.
 */
async function main() {
  MOONCAT_LOOTPRINTS_ADDRESS = (await MoonCatLootprints).address;

  for (let slot = 0; slot < 650; slot++) {
    let rs = await getSlotData(slot);
    let slotLabel = ('0000'+slot).slice(-4);

    let stringData = (slot < 150) ? asString(rs) : '';

    console.log(slotLabel, rs, stringData);
  }
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
