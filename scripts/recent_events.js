require('dotenv').config();
const { ethers } = require('hardhat');
const { MoonCatAcclimator } = require('@mooncatrescue/contracts/moonCatUtils');

const { FORK_BLOCK } = process.env;

async function main() {
  const ACCLIMATOR = await MoonCatAcclimator;

  // Find events that were fired recently from the contracts
  const eventFilter = {
    address: ACCLIMATOR.address,
    topics: [[
      ethers.utils.id('MoonCatAcclimated(uint256,address)'),
      ethers.utils.id('MoonCatDeacclimated(uint256,address)'),
    ]]
  };
  //console.log(ACCLIMATOR.interface.events);

  let moonCats = {};
  let addresses = {};

  let logs = await ACCLIMATOR.queryFilter(eventFilter, FORK_BLOCK - 100_000);
  console.log(`${logs.length} events needing to be parsed:`)
  for(let i = 0; i < logs.length; i++) {
    let event = ACCLIMATOR.interface.parseLog(logs[i]);
    let block = await logs[i].getBlock();
    let blockTime = new Date(block.timestamp * 1000);
    let rescueOrder = event.args.tokenId.toNumber();
    moonCats[rescueOrder] = true;
    addresses[event.args.owner] = true;
    console.log(`${event.name}: MoonCat #${rescueOrder}, by ${event.args.owner} in block ${block.number} (${blockTime.toLocaleString()})`);
  }

  moonCats = Object.keys(moonCats).sort((a, b) => { a - b });
  addresses = Object.keys(addresses).sort();

  console.log('');
  console.log(`${moonCats.length} MoonCats Acclimated:`);
  console.log(moonCats.join("\n"));
  console.log('');
  console.log(`${addresses.length} unique addresses triggered Acclimation`);
  console.log(addresses.join("\n"));
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
