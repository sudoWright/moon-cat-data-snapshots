require('dotenv').config();
const { ethers } = require('hardhat');
const { writeFileAsync } = require('../lib/fileUtils');
const { MoonCatAcclimator } = require('@mooncatrescue/contracts/moonCatUtils');

let MOONCAT_ACCLIMATOR_ADDRESS = false;

// https://medium.com/better-programming/solidity-storage-variables-with-ethers-js-ca3c7e2c2a64

// Acclimator memory slot numbering:
// ===================================
// 0: _holderTokens mapping
// 1-3: _tokenOwners mapping (EnumerableMap.UintToAddressMap)
//   2: _entries; array of MapEntry structs
//   3: _indexes; mapping of keys to index values in _entries
// 4: _tokenApprovals mapping
// 5: _operatorApprovals mapping
// 6: Token Name
// 7: Token Symbol
// 8: _tokenURIs mapping
// 9: _baseURI string
// 10: Owner address
// 11: Rescue order to old-wrapper-id mapping contract address
// 12: MoonCatRescue Address
// 13: Old Wrapper Address

const abi = ethers.utils.defaultAbiCoder;

async function getSlotData(slot) {
  const paddedSlot = ethers.utils.hexZeroPad(slot, 32);
  const storageData = await ethers.provider.getStorageAt(MOONCAT_ACCLIMATOR_ADDRESS, paddedSlot);
  return storageData;
}

function asString(data) {
  let trimmed = ethers.BigNumber.from(data).and(ethers.constants.MaxInt256.sub(255)).toHexString();
  try {
    return ethers.utils.toUtf8String(trimmed).replace(/\x00/g, '');
  } catch (err) {
    return '';
  }
}

/**
 * Generate a `${MOONCAT_ACCLIMATOR_ADDRESS}_memory.json` cache file for faster ownership lookups.
 * Rather than looping through calling "ownerOf()" for each MoonCat (having the EVM look up
 * the data in memory, parse, and format it), record where in smart contract memory that data is
 * stored (which doesn't change). With that information generated, ownership can be determined by
 * looking up the raw memory value (a faster RPC call).
 *
 * This wrapping contract assigns MoonCats a position in the ownership array as they become wrapped,
 * so if a MoonCat has never been wrapped it has an indeterminate position. Therefore, this script
 * will need to be run periodically, to update the cache file with newly-acclimated MoonCats.
 */
async function main() {
  MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;

  // Show the first 15 memory slots, for debugging
  for (let slot = 0; slot < 15; slot++) {
    let rs = await getSlotData(slot);
    let slotLabel = ('0000'+slot).slice(-4);

    let stringData = (slot < 150) ? asString(rs) : '';

    console.log(slotLabel, rs, stringData);
  }

  console.log('');
  const entriesStartSlot = ethers.utils.keccak256(abi.encode(['uint256'], [2])); // Dynamic arrays start at the hash of their slot
  /*
  console.log('entries test');
  for (let i = 0; i < 50; i++) {
    let slot = BigNumber.from(entriesStartSlot).add(i).toHexString();
    let slotData = await ethers.provider.getStorageAt(MOONCAT_ACCLIMATOR_ADDRESS, slot);
    console.log(i, slot, slotData);
  }
  console.log('');
  */

  let acclimator_memory = [];
  // Set all of them to default as having no data
  for (let rescueOrder = 0; rescueOrder < 25440; rescueOrder++) {
    acclimator_memory[rescueOrder] = false;
  }

  // Loop through all the contract entries in index-order (which will not be rescue-order)
  const mappingLength = ethers.BigNumber.from(await getSlotData(2)).toNumber();
  console.log(`Finding ${mappingLength} index values...`);
  const entrySize = 2; // Number of slots each entry takes up
  for(let i = 0; i < mappingLength; i++) {
    let entrySlot = ethers.BigNumber.from(entriesStartSlot).add(i * entrySize);
    let rescueOrder = abi.decode(
      ['uint256'],
      await ethers.provider.getStorageAt(MOONCAT_ACCLIMATOR_ADDRESS, entrySlot.toHexString())
    )[0].toNumber();
    acclimator_memory[rescueOrder] = {
      rescueOrder: rescueOrder,
      ownerSlot: entrySlot.add(1).toHexString(),
      approvalSlot: ethers.utils.keccak256(abi.encode(['uint256', 'uint256'], [rescueOrder, 4]))
    }
    if (i % 500 == 0) {
      slotData = await ethers.provider.getStorageAt(
        MOONCAT_ACCLIMATOR_ADDRESS,
        acclimator_memory[rescueOrder].ownerSlot
      );
      let ownerAddress = abi.decode(['address'], slotData)[0];
      console.log(i, rescueOrder, ownerAddress);
    }
  }
  await writeFileAsync(`${MOONCAT_ACCLIMATOR_ADDRESS}_memory.json`, JSON.stringify(acclimator_memory, null, 2));

  console.log('Done!');
}


main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
