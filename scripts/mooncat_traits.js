const { ethers } = require('hardhat');
const { writeFileAsync } = require('../lib/fileUtils');

/**
 * Generate a mooncat_traits.json file with the static data about each MoonCat.
 */
async function main() {
  const accounts = await ethers.getSigners();
  const MULTI = new ethers.Contract('0xcA11bde05977b3631167028862bE2a173976CA11', [
    // https://github.com/mds1/multicall
    'function aggregate3(tuple(address target, bool allowFailure, bytes callData)[] calls) external view returns (tuple(bool success, bytes returnData)[] returnData)',
  ], accounts[0]);
  const TRAITS = new ethers.Contract('0x9330BbfBa0C8FdAf0D93717E4405a410a6103cC2', [
    'function traitsOf (uint256 rescueOrder) public view returns (bool genesis, bool pale, string memory facing, string memory expression, string memory pattern, string memory pose, bytes5 catId, uint16 rescueYear, bool isNamed)'
  ], accounts[0]);

  let mooncat_traits = [];
  const CHUNK_SIZE = 250;
  for (let i = 0; i < 25440; i += CHUNK_SIZE) {
    console.log(`At ${i}...`);
    let callDatas = [];
    for (let j = 0; j < CHUNK_SIZE; j++) {
      if (i + j >= 25440) break;
      let cd = (await TRAITS.populateTransaction.traitsOf(i + j)).data;
      callDatas.push({
        target: TRAITS.address,
        allowFailure: false,
        callData: cd
      });
    }
    let rs = await MULTI.aggregate3(callDatas);
    for (let j = 0; j < rs.length; j++) {
      let r = rs[j];
      if (r.success !== true) {
        console.error('Trait fetch error');
        console.log(r);
        process.exit(1);
      }
      let traits = TRAITS.interface.decodeFunctionResult('traitsOf', r.returnData);
      let data = {
        rescueOrder: i + j,
        catId: traits.catId,
        genesis: traits.genesis,
        pale: traits.pale,
        facing: traits.facing,
        expression: traits.expression,
        pattern: traits.pattern,
        pose: traits.pose,
        rescueYear: traits.rescueYear,
      };
      mooncat_traits.push(data);
    }
    await writeFileAsync('mooncat_traits.json', JSON.stringify(mooncat_traits, null, 2));
  }
  await writeFileAsync('mooncat_traits.json', JSON.stringify(mooncat_traits, null, 2));
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
