require('dotenv').config();
const fs = require('fs');

const { MOONCAT_ACCLIMATOR_ADDRESS, FORK_BLOCK, OTHER_OWNERS } = process.env;

/**
 * Given an input list of addresses (OTHER_OWNERS; who own a different asset for a collaboration),
 * iterate through the list of MoonCat owners and find those that overlap.
 */
(async () => {
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const mooncat_owners = require('../' + MOONCAT_OWNERS_FILE);
  const other_owners = fs.readFileSync(OTHER_OWNERS).toString().split("\n");

  let eligibleMoonCats = [];
  let eligibleOwners = Object.keys(mooncat_owners).filter(ownerAddress => {
    return other_owners.indexOf(ownerAddress) >= 0;
  }).sort();
  console.log(`MoonCat Owners (${eligibleOwners.length}):`);
  eligibleOwners.forEach(ownerAddress => {
    console.log(ownerAddress);
    eligibleMoonCats = eligibleMoonCats.concat(mooncat_owners[ownerAddress]);
  });

  console.log('');
  console.log(`Eligible MoonCats (${eligibleMoonCats.length}):`);
  console.log(eligibleMoonCats.sort((a,b) => a - b).join(','));
})();
