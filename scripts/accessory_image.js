require('dotenv').config();
const fs = require('fs');
const { ethers } = require('hardhat');
const { MoonCatAccessories } = require('@mooncatrescue/contracts/moonCatUtils');

async function main() {
  const accounts = await ethers.getSigners();
  const ACC = new ethers.Contract('0x91CF36c92fEb5c11D3F5fe3e8b9e212f7472Ec14', [
    'function accessoryPNG (uint256 rescueOrder, uint256 accessoryId, uint16 paletteIndex) external view returns (string)'
  ]).connect(accounts[0]);
  const ACCESSORIES = await MoonCatAccessories;

  async function saveAccessoryImage(accessoryId) {
    let data = await ACC.accessoryPNG(0, accessoryId, 0);
    let commaPos = data.indexOf(',');
    let imgData = Buffer.from(data.slice(commaPos + 1), 'base64');
    let accessoryInfo = await ACCESSORIES.accessoryInfo(accessoryId);
    let accessoryName = ethers.utils.toUtf8String(accessoryInfo.name).replace(/\x00/g, '');

    let filename = `accessory-${accessoryId}_${accessoryName}.png`;
    fs.writeFileSync(filename, imgData);
    return filename;
  }


  for (accessoryId of [0, 1, 36, 198, 311, 319, 333, 563, 564, 572, 657, 755, 834, 900, 901, 918]) {
    let filename = await saveAccessoryImage(accessoryId);
    console.log('Saved', filename);
  }
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
