require('dotenv').config();
const { MoonCatAcclimator, MoonCatLootprints } = require('@mooncatrescue/contracts/moonCatUtils');

/*
Using the Snapshot.org GraphQL endpoints, data can be fetched about a specific proposal and its results:

query Proposal {
  proposal(id:"0x4d550659fe9b63a7ac0561b4c131097856544e995e301d7a36fd677ab06cb7f8") {
    id
    title
    body
    choices
    start
    end
    snapshot
    state
    author
    created
    scores
    scores_by_strategy
    scores_total
    scores_updated
    plugins
    network
    strategies {
      name
      network
      params
    }
    space {
      id
      name
    }
  }
}

query Votes {
  votes (
    first: 1000
    where: {
      proposal: "0x4d550659fe9b63a7ac0561b4c131097856544e995e301d7a36fd677ab06cb7f8"
    }
    orderBy: "created",
    orderDirection: desc
  ) {
    id
    voter
    vp
    vp_by_strategy
    created
    choice
  }
}

That generates JSON data; this script then converts that into CSV format
*/


async function main() {
  let { data } = require('../001_accessories-off.json');
  const ACCLIMATOR = await MoonCatAcclimator;
  const LOOTPRINTS = await MoonCatLootprints;
  const choices = [
    "null",
    "Accessories ON by default",
    "Vetted Accessories ON by default",
    "Accessories OFF by default"
  ];
  console.log('ID,Voter,Points,Points by Strategy,Vote Date,Choice,MoonCats,lootprints');

  for (let i = 0; i < data.votes.length; i++) {
    const vote = data.votes[i];

    // Determine how many MoonCats the address has
    vote.mooncats = (await ACCLIMATOR.balanceOf(vote.voter)).toNumber();

    // Determine how many lootprints the address has
    vote.lootprints = (await LOOTPRINTS.balanceOf(vote.voter)).toNumber();

    let votedate = new Date(vote.created * 1000);

    console.log(`${vote.id},${vote.voter},${vote.vp},${vote.vp_by_strategy.join(':')},"${votedate.toLocaleString()}","${choices[vote.choice]}",${vote.mooncats},${vote.lootprints}`);
    //console.log(vote);
  }
}

main()
.then(() => process.exit(0))
.catch(error => {
  console.error(error);
  process.exit(1);
});
