require('dotenv').config();
const { MoonCatAcclimator, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');
const { FORK_BLOCK } = process.env;

const SECONDS_IN_DAY = 60 * 60 * 24;
const ACTIVITY_FILE = 'address_activity.json';

(async () => {
  const MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  const MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;
  const WILDMOONCAT_OWNERS_FILE = `${MOONCATRESCUE_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  let wildmooncat_owners = require('./' + WILDMOONCAT_OWNERS_FILE);
  let mooncat_owners = require('./' + MOONCAT_OWNERS_FILE);

  // Combine wild and acclimated owners into one list
  let merged_owners = Object.assign({}, mooncat_owners);
  Object.keys(wildmooncat_owners).forEach(ownerAddress => {
    if (typeof merged_owners[ownerAddress] == 'undefined') {
      merged_owners[ownerAddress] = wildmooncat_owners[ownerAddress];
    } else {
      merged_owners[ownerAddress] = merged_owners[ownerAddress].concat(wildmooncat_owners[ownerAddress])
    }
  });

  let ownerActivity = require('./' + ACTIVITY_FILE);
  const now = new Date();
  const owners = Object.keys(merged_owners);

  function showOwnersActiveSince(maxSecondsAgo) {
    let numDays = Math.floor(maxSecondsAgo / SECONDS_IN_DAY);
    let minTimestamp = (now.getTime() / 1000) - maxSecondsAgo;
    let activeOwners = owners.filter(ownerAddress => {
      return ownerActivity[ownerAddress] >= minTimestamp
    });
    console.log(`Active owner accounts in the last ${numDays} days: ${activeOwners.length.toLocaleString()}`);
    let activeMoonCatCount = activeOwners.reduce((sum, ownerAddress) => merged_owners[ownerAddress].length + sum, 0);
    console.log(`Active MoonCats: ${activeMoonCatCount.toLocaleString()}`);
  }

  showOwnersActiveSince(SECONDS_IN_DAY * 120); // 120 days
  console.log('')
  showOwnersActiveSince(SECONDS_IN_DAY * 365); // 365 days
})();