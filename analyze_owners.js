require('dotenv').config();
const { MoonCatAcclimator, MoonCatLootprints, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');

const { FORK_BLOCK } = process.env;
let MOONCAT_ACCLIMATOR_ADDRESS = MOONCAT_LOOTPRINTS_ADDRESS = MOONCATRESCUE_ADDRESS = false;
const MOONCATPOP_ADDRESS = '0xb8c11beda7142ae7986726247f548eb0c3cde474';
const MOONCATPOPVM_ADDRESS = '0x09C61c41C8C5D378CAd80523044C065648Eaa654';

function countTokens(ownersObj) {
  let totalCount = 0;
  Object.keys(ownersObj).forEach(ownerAddress => {
    totalCount += ownersObj[ownerAddress].length;
  });
  return totalCount;
}

function filterByCount(ownersObj, minBalance) {
  let parsed = {};
  Object.keys(ownersObj).forEach(ownerAddress => {
    if (ownersObj[ownerAddress].length >= minBalance) {
      parsed[ownerAddress] = ownersObj[ownerAddress];
    }
  });
  return parsed;
}

function filterByAddress(ownersObj, toRemove) {
  toRemove.forEach(ownerAddress => {
    delete ownersObj[ownerAddress];
  });
  return ownersObj;
}


(async () => {
  MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  MOONCAT_LOOTPRINTS_ADDRESS = (await MoonCatLootprints).address;
  MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;

  const WILDMOONCAT_OWNERS_FILE = `${MOONCATRESCUE_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const LOOTPRINT_OWNERS_FILE = `${MOONCAT_LOOTPRINTS_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const POP_OWNERS_FILE = `${MOONCATPOP_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const VM_OWNERS_FILE = `${MOONCATPOPVM_ADDRESS}_${FORK_BLOCK}_owners.json`;

  let wildmooncat_owners = require('./' + WILDMOONCAT_OWNERS_FILE);
  let mooncat_owners = require('./' + MOONCAT_OWNERS_FILE);
  let lootprint_owners = require('./' + LOOTPRINT_OWNERS_FILE);
  let pop_owners = require('./' + POP_OWNERS_FILE);
  let vm_owners = require('./' + VM_OWNERS_FILE);

  let wildMoonCatCount = countTokens(wildmooncat_owners);
  let moonCatCount = countTokens(mooncat_owners);
  let lootprintCount = countTokens(lootprint_owners);
  let popCount = countTokens(pop_owners);
  let vmCount = countTokens(vm_owners);

  console.log(`Analysis from block ${FORK_BLOCK}:`);
  console.log(`Wild MoonCat Owners: ${Object.keys(wildmooncat_owners).length}, Total MoonCats: ${wildMoonCatCount}`);
  console.log(`Acclimated MoonCat Owners: ${Object.keys(mooncat_owners).length}, Total MoonCats: ${moonCatCount}`);
  console.log(`lootprint Owners: ${Object.keys(lootprint_owners).length}, Total lootprints: ${lootprintCount}`);
  console.log(`Pop Owners: ${Object.keys(pop_owners).length}, Total Pop cans: ${popCount}`);
  console.log(`Vending Machine Owners: ${Object.keys(vm_owners).length}, Total vending machines: ${vmCount}`);

  // Merge unique owners across Acclimated MoonCats, lootprints, pop, and Vending Machines.
  let uniqueOwners = {};
  Object.keys(mooncat_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);
  Object.keys(lootprint_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);
  Object.keys(pop_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);
  Object.keys(vm_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);

  console.log('');
  console.log(`Unique addresses across Acclimated, lootprint, pop, and vending machine collections: ${Object.keys(uniqueOwners).length}`);

  // Find large-holders who didn't vote
  let { data } = require('./001_accessories-off.json');
  let didVote = data.votes.map(vote => vote.voter);

  let largeMoonCatOwners = filterByAddress(filterByCount(mooncat_owners, 5), didVote);
  let largeLootprintOwners = filterByAddress(filterByCount(lootprint_owners, 5), didVote);
  let largePopOwners = filterByAddress(filterByCount(pop_owners, 10), didVote);
  let largeVmOwners = filterByAddress(filterByCount(vm_owners, 1), didVote);

  console.log('');
  console.log(`There were ${Object.keys(largeMoonCatOwners).length} MoonCat holders with 5 or more MoonCats who didn't vote`);
  console.log(`There were ${Object.keys(largeLootprintOwners).length} lootprint holders with 5 or more lootprints who didn't vote`);
  console.log(`There were ${Object.keys(largePopOwners).length} MoonCatPop holders with 10 or more Pop cans who didn't vote`);
  console.log(`There were ${Object.keys(largeVmOwners).length} Vending Machine holders who didn't vote`);
  //console.log(Object.keys(largeMoonCatOwners));

})();