Tools and scripts for working blockchain data, taken as snapshots at different block heights.

This is useful for working with Snapshot.org proposals (which always are pegged to a specific block height), as well as using specific block heights as eligibility for other integrations and collaborations

# Snapshot.org
To get data out of Snapshot, use their GraphQL endpoint([documentation](https://docs.snapshot.org/graphql-api), [endpoint](https://hub.snapshot.org/graphql)). To use it, you'll need to get the ID of the Proposal. That can be done by either using the web UI and getting it out of the URL, or using the GraphQL endpoint with:

```graphql
query Proposals {
  proposals (
    where:{
      space: "mooncatcommunity.eth"
    }
  ) {
    id
    created
    title
    body
    snapshot
  }
}
```

Once you have the Proposal's ID, you can insert the `snapshot` value into your `.env` file as the `FORK_BLOCK`, and then run through the scripts to parse ownership at that block height.

## MoonCat-related Proposals

- MoonCatCommunity.eth: [Accessories on/vetted/off](https://snapshot.org/#/mooncatcommunity.eth/proposal/0x4d550659fe9b63a7ac0561b4c131097856544e995e301d7a36fd677ab06cb7f8). Block 14654573
- MEAO.eth: [MoonCat Community Vote on Accessories](https://snapshot.org/#/meao.eth/proposal/0xd0e15246eeb5743f2bc677a951b71e7148ba50c117eb610e6c025decc0acd2bd). Block 16191000

# Get owners at a specific time
First, figure out which Ethereum block corresponds to that time. For the desired time, calculate the [Unix epoch time](https://www.epochconverter.com/) for that time. Then insert it into this API query against Etherscan's data to find the last block mined before that timestamp passed:

    https://api.etherscan.io/api?module=block&action=getblocknobytime&timestamp=1653336000&closest=before

That block number needs to be then passed in as the `FORK_BLOCK` environment variable for Hardhat to fork from. You can either set it in your `.env` or specify it on the command line like `FORK_BLOCK=14831517 npx hardhat run scripts/owners.js`

The `scripts/owners.js` script iterates through wild and Acclimated MoonCats, and records proper ownership of each (the Acclimator contracts are excluded from being enumerated as owner in the 'wild' output). It also enumerates lootprints owners.

The `scripts/popowners.js` script iterates through MoonCatPop Cans and Vending Machines and records their owners.

## Notable block heights

- 13913265: Dec-31-2021 12:59:59 PM +UTC, Last block of 2021 calendar year
- 14654573: Apr-25-2022 03:28:00 PM +UTC, Accessories on/vetted/off Snapshot vote
- 14831517: May-23-2022 08:00:00 PM +UTC, Realms of Ether accessory collaboration
- 16191000: Dec-15-2022 03:30:23 PM +UTC, MEAO Accessories Snapshot vote

# Owner iteration methods timing
Running the `run_timings.sh` script will run several tests on batches of 100 assets to see how long they take with different iteration methods. It will produce an output similar to:

```
wild batch: 9.985s
acclimated batch: 17.740s
lootprints ownerof batch: 11.332s
lootprints details batch: 32.641s

wild loop: 11.420s
wild loop2: 9.264s
acclimated loop: 19.292s
lootprints ownerof loop: 9.166s
lootprints details loop: 31.050s

wild memory: 6.589s
wild multi: 11.698s
```

The script is needed to separate the runs from each other as much as possible, to ensure Hardhat is working from a "cold cache" for each run (otherwise test runs that happen later in the ordering would have an advantage of working off of some cached data).

The "batch" tests use the `BulkOwners.sol` helper contract, to move the looping/iteration into Solidity, while the "loop" tests use looping within Javascript to iterate over the set of IDs.

These tests show that in general, using the `BulkOwners.ownersOf()` method is slightly faster than iterating directly over the contract functions, but doing raw memory lookups is even faster. Using a generic "multicall" contract helps reduce RPC calls, but doesn't provide a significant speed boost.
