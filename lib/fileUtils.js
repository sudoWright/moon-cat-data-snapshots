const fs = require('fs');

function writeFileAsync(file, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(file, data, err => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

function initializeFile(filename) {
  try {
    fs.writeFileSync(filename, '{}', { flag: 'wx' });
  } catch (err) {
    if (err.code == 'EEXIST') {
      // File already exists; no issue
      return
    }
    console.error(err);
  }
}


module.exports = {
  initializeFile,
  writeFileAsync
}