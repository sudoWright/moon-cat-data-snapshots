const { ethers } = require('ethers');
const { MoonCatAcclimator, MoonCatLootprints, MoonCatRescue } = require('@mooncatrescue/contracts/moonCatUtils');

require('dotenv').config();

const { FORK_BLOCK, API_URL } = process.env;
let MOONCAT_ACCLIMATOR_ADDRESS = MOONCAT_LOOTPRINTS_ADDRESS = MOONCATRESCUE_ADDRESS = false;
const MOONCATPOP_ADDRESS = '0xb8c11beda7142ae7986726247f548eb0c3cde474';
const MOONCATPOPVM_ADDRESS = '0x09C61c41C8C5D378CAd80523044C065648Eaa654';


(async () => {
  MOONCAT_ACCLIMATOR_ADDRESS = (await MoonCatAcclimator).address;
  MOONCAT_LOOTPRINTS_ADDRESS = (await MoonCatLootprints).address;
  MOONCATRESCUE_ADDRESS = (await MoonCatRescue).address;

  const WILDMOONCAT_OWNERS_FILE = `${MOONCATRESCUE_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const MOONCAT_OWNERS_FILE = `${MOONCAT_ACCLIMATOR_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const LOOTPRINT_OWNERS_FILE = `${MOONCAT_LOOTPRINTS_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const POP_OWNERS_FILE = `${MOONCATPOP_ADDRESS}_${FORK_BLOCK}_owners.json`;
  const VM_OWNERS_FILE = `${MOONCATPOPVM_ADDRESS}_${FORK_BLOCK}_owners.json`;

  let wildmooncat_owners = require('./' + WILDMOONCAT_OWNERS_FILE);
  let mooncat_owners = require('./' + MOONCAT_OWNERS_FILE);
  let lootprint_owners = require('./' + LOOTPRINT_OWNERS_FILE);
  let pop_owners = require('./' + POP_OWNERS_FILE);
  let vm_owners = require('./' + VM_OWNERS_FILE);

  const provider = new ethers.providers.JsonRpcProvider(API_URL);
  async function showAddress(address) {
    let ens = await provider.lookupAddress(address);
    if (ens != null) {
      console.log(address, '=>', ens);
    } else {
      console.log(address);
    }
  }

  mooncat_owners = Object.keys(mooncat_owners).sort();
  console.log('');
  console.log(`Acclimated MoonCat owners: ${mooncat_owners.length}`);
  for(let i = 0; i < mooncat_owners.length; i++) {
    await showAddress(mooncat_owners[i]);
  }

  let uniqueOwners = {};
  Object.keys(pop_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);
  Object.keys(vm_owners).forEach(ownerAddress => uniqueOwners[ownerAddress] = true);
  uniqueOwners = Object.keys(uniqueOwners).sort();

  console.log('');
  console.log(`Unique addresses across pop and vending machine collections: ${uniqueOwners.length}`);
  for(let i = 0; i < uniqueOwners.length; i++) {
    await showAddress(uniqueOwners[i]);
  }
  console.log(uniqueOwners.join("\n"));

})();